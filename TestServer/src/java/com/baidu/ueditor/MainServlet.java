package com.baidu.ueditor;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * copyright (C), 2018-2018, 创蓝253
 * fileName MainServlet
 * author   zhangx
 * date     2018/9/29 17:05
 * description
 */
public class MainServlet extends GenericServlet{

    //    out.write( new ActionEnter( request, rootPath ).exec() );

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        HttpServletRequest request;
        HttpServletResponse response;
        try {
            request = (HttpServletRequest)servletRequest;
            response = (HttpServletResponse)servletResponse;
        } catch (ClassCastException var6) {
            throw new ServletException("non-HTTP request or response");
        }

        request.setCharacterEncoding( "utf-8" );
        response.setHeader("Content-Type" , "text/html");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Authorization,X-Requested-With,X_Requested_With");
        response.setHeader("Access-Control-Allow-Credentials","true");

        ServletOutputStream out = response.getOutputStream();
        String rootPath="D:/data/center_data/oem_news";
        String result=new ActionEnter( request, rootPath ).exec();

        out.print(result );



    }





}
