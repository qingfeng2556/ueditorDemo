# ueditorDemo

#### 项目介绍
百度ueditorDemo，源码是ueditor-1.4.3.3 jsp版

#### 软件架构
软件架构说明
TestWeb为页面部分，TestServer为java后台服务器

#### 使用说明

1. 修改ip地址端口为自己的，TestWeb、TestServer分别放到项目中，在tomcat中启动 ，可在windows下运行
2. 这些代码跟源码有些不同，serve部分主要修改了路径，添加了一个Servlet，路径设置
3. web部分参考csdn博文进行了修改（
	https://blog.csdn.net/qq_15766181/article/details/79914370 
	https://blog.csdn.net/u012453843/article/details/60583226）
4、图片视频前缀使用nginx指向本地目录
5、单图片跨域上传失败问题，需要自己改web源码
6、https://github.com/yueweicai/uediter-front 针对单张图片不能上传解决问题。
#### 参与贡献

作者 CSDN 清风2556

